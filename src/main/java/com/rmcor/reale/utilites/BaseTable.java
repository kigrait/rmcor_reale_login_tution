package com.rmcor.reale.utilites;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author shailendrayadav
 */
@MappedSuperclass
@SuppressWarnings("ConsistentAccessType")
public abstract class BaseTable {

	public final static String TABLE_NAME = "BaseTable";
	public static final String KEY_ID = "_id";
	public static final String KEY_ISSYNC = "issync";
	public static final String KEY_ISACTIVE = "isactive";
	public static final String KEY_CREATED_BY = "created_by";
	public static final String KEY_CREATED_DATE = "created_date";
	public static final String KEY_UPDATED_BY = "updated_by";
	public static final String KEY_UPDATED_DATE = "updated_date";
	public static final String KEY_SYNC_DATE = "sync_date";

	public static final String KEY_CREATED_BY_ID = "created_by_id";
	public static final String KEY_UPDATED_BY_ID = "updated_by_id";
	public static final String KEY_DIN = "din";

	@Column(name = KEY_ISACTIVE)
	protected Integer isactive;
	
	@Column(name = KEY_CREATED_BY, updatable = false)
	protected String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = KEY_CREATED_DATE, length = 35, updatable = false)
	protected Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = KEY_UPDATED_DATE, length = 35)
	protected Date updatedDate;
	
	@Column(name = KEY_UPDATED_BY)
	protected String updatedBy;
	
	@Column(name = KEY_SYNC_DATE)
	protected Long syncDate;
	
	@Column(name = KEY_DIN)
	protected String din;

	@Column(name = KEY_CREATED_BY_ID)
	protected Long createdById;
	
	@Column(name = KEY_UPDATED_BY_ID)
	protected Long updatedById;
	
	@Column(name = "orig_src_sys")
	protected String origSrcSys;
	
	@Column(name = "prev_src_sys")
	protected String prevSrcSys;
	
	@Column(name = "created_from_ip")
	protected String createdFromIp;

	@Column(name = "created_from_mac_id")
	protected String createdFromMacId;

	@Column(name = "updated_from_ip")
	protected String updatedFromIp;

	@Column(name = "updated_from_mac_id")
	protected String updatedFromMacId;

	@Column(name = "status")
	protected String status;

	@Column(name = "country")
	protected String country;

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getSyncDate() {
		return this.syncDate;
	}

	public void setSyncDate(Long syncDate) {
		this.syncDate = syncDate;
	}

	public String getDin() {
		return this.din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public Long getCreatedById() {
		return this.createdById;
	}

	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}

	public Long getUpdatedById() {
		return this.updatedById;
	}

	public void setUpdatedById(Long updatedById) {
		this.updatedById = updatedById;
	}

	public String getOrigSrcSys() {
		return this.origSrcSys;
	}

	public void setOrigSrcSys(String origSrcSys) {
		this.origSrcSys = origSrcSys;
	}

	public String getPrevSrcSys() {
		return this.prevSrcSys;
	}

	public void setPrevSrcSys(String prevSrcSys) {
		this.prevSrcSys = prevSrcSys;
	}

	public String getCreatedFromIp() {
		return this.createdFromIp;
	}

	public void setCreatedFromIp(String createdFromIp) {
		this.createdFromIp = createdFromIp;
	}

	public String getCreatedFromMacId() {
		return this.createdFromMacId;
	}

	public void setCreatedFromMacId(String createdFromMacId) {
		this.createdFromMacId = createdFromMacId;
	}

	public String getUpdatedFromIp() {
		return this.updatedFromIp;
	}

	public void setUpdatedFromIp(String updatedFromIp) {
		this.updatedFromIp = updatedFromIp;
	}

	public String getUpdatedFromMacId() {
		return this.updatedFromMacId;
	}

	public void setUpdatedFromMacId(String updatedFromMacId) {
		this.updatedFromMacId = updatedFromMacId;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public abstract void setTableId(Long tableId);

	@Transient
	public abstract Long getTableId();

	@Transient
	public String getTableIdKey() {
		return "pid";
	}

	@Transient
	public String getTableName() {
		Class c = this.getClass();
		Table t = (Table) c.getAnnotation(Table.class);
		return t.name();
	}
}
