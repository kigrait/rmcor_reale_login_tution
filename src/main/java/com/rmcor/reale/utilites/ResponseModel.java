package com.rmcor.reale.utilites;

import java.util.HashMap;
import java.util.Map;

public class ResponseModel {

	private String status;
	private String message;
	private Map<String, Object> response;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getResponse() {
		return response;
	}

	public void setResponse(Map<String, Object> response) {
		this.response = response;
	}

}
