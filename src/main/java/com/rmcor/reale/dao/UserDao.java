package com.rmcor.reale.dao;
/**
*
* @author Shailendra yadav
*/
import org.springframework.stereotype.Repository;

import com.rmcor.reale.hibernate.model.ContactUs;
import com.rmcor.reale.hibernate.model.Login;
import com.rmcor.reale.hibernate.model.Registration;
import com.rmcor.reale.hibernate.model.RmcorContactUs;
import com.rmcor.reale.model.LoginModel;
import com.rmcor.reale.model.RegistrationModel;

@Repository
public interface UserDao {

	public Registration saveUserRegistration(Registration registration);

	public String saveLoginDetail(Login login);

	public Integer checkMobileNumber(RegistrationModel registrationModel);

	public Integer checkEmailId(RegistrationModel registrationModel);

	public Login userLogin(LoginModel loginModel);

	public ContactUs saveContactUs(ContactUs contactUs);

	public RmcorContactUs saveRmcorContactUs(RmcorContactUs contactUs);


}
