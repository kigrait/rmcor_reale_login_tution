package com.rmcor.reale.model;
/**
*
* @author Shailendra yadav
*/
public class LoginModel implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long pid;
	private Long registeredUserId;
	private String userName;
	private String emailId;
	private String mobileNumber;
	private String password;

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getRegisteredUserId() {
		return registeredUserId;
	}

	public void setRegisteredUserId(Long registeredUserId) {
		this.registeredUserId = registeredUserId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginModel [pid=" + pid + ", registeredUserId=" + registeredUserId + ", userName=" + userName
				+ ", emailId=" + emailId + ", mobileNumber=" + mobileNumber + ", password=" + password + "]";
	}


}
