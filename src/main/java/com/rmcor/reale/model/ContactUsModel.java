package com.rmcor.reale.model;

public class ContactUsModel implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private Long pid;
	private Long registeredUserId;
	private String name;
	private String emailId;
	private String country;
	private String subject;
	private String mobileNumber;
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public Long getRegisteredUserId() {
		return registeredUserId;
	}
	public void setRegisteredUserId(Long registeredUserId) {
		this.registeredUserId = registeredUserId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	
	
}
