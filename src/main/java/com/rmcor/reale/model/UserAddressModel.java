package com.rmcor.reale.model;
/**
*
* @author Shailendra yadav
*/
public class UserAddressModel implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long pid;
	private Long registeredUserId;
	private String careOfName;
	private String street1;
	private String street2;
	private String street3;
	private String village;
	private String city;
	private String post;
	private String region;
	private String district;
	private String state;
	private String country;
	private String pinCode;
	private String addressType;
	private String addressPriority;

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getRegisteredUserId() {
		return registeredUserId;
	}

	public void setRegisteredUserId(Long registeredUserId) {
		this.registeredUserId = registeredUserId;
	}

	public String getCareOfName() {
		return careOfName;
	}

	public void setCareOfName(String careOfName) {
		this.careOfName = careOfName;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getStreet3() {
		return street3;
	}

	public void setStreet3(String street3) {
		this.street3 = street3;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressPriority() {
		return addressPriority;
	}

	public void setAddressPriority(String addressPriority) {
		this.addressPriority = addressPriority;
	}



}
