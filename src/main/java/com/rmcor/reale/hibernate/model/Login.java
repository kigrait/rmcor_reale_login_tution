package com.rmcor.reale.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "login", schema = "rmcor_mysql_database")
public class Login implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pid;
	@Column(name = "registeredUserId")
	private Long registeredUserId;
	@Column(name = "userName")
	private String userName;
	@Column(name = "emailId")
	private String emailId;
	@Column(name = "mobileNumber")
	private String mobileNumber;
	@Column(name = "password")
	private String password;
	@Column(name = "role")
	private String role;
	@Column(name = "dateTime")
	private String dateTime;
	@Column(name = "isActive")
	private int isActive;


	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public Long getRegisteredUserId() {
		return registeredUserId;
	}

	public void setRegisteredUserId(Long registeredUserId) {
		this.registeredUserId = registeredUserId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Login [pid=" + pid + ", registeredUserId=" + registeredUserId + ", userName=" + userName + ", emailId="
				+ emailId + ", mobileNumber=" + mobileNumber + ", password=" + password + ", dateTime=" + dateTime
				+ ", isActive=" + isActive + "]";
	}


	
	
}
