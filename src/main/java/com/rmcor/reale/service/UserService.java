package com.rmcor.reale.service;
import com.rmcor.reale.model.ContactUsModel;
import com.rmcor.reale.model.LoginModel;
/**
*
* @author Shailendra yadav
*/
import com.rmcor.reale.model.RegistrationModel;
import com.rmcor.reale.utilites.ResponseModel;

public interface UserService {

	public ResponseModel saveUserRegistration(RegistrationModel registrationModel);

	public ResponseModel userLogin(LoginModel loginModel);

	public ResponseModel saveContactUs(ContactUsModel contactUsModel);

	public ResponseModel saveRmcorContactUs(ContactUsModel contactUsModel);

}
