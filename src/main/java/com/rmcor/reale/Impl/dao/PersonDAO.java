package com.rmcor.reale.Impl.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rmcor.reale.hibernate.model.Person;


@Repository
@Transactional
public class PersonDAO {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getSession() {
		Session session = sessionFactory.getCurrentSession();
		if(session==null) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	public String savePerson(Person person) {
		System.out.println("happy11111111111111");
		Long isSuccess = (Long) getSession().save(person);
		if (isSuccess >= 1) {
			return "Success";
		} else {
			return "Error while Saving Person";
		}

	}

	public boolean delete(Person person) {
		getSession().delete(person);
		return true;
	}

	@SuppressWarnings("unchecked")
	public List getAllPersons() {
		return getSession().createQuery("from Person").list();
	}
}
