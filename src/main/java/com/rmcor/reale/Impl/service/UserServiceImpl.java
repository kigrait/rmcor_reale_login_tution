package com.rmcor.reale.Impl.service;
/**
*
* @author Shailendra yadav
*/
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rmcor.reale.Impl.util.SendMail;
import com.rmcor.reale.dao.UserDao;
import com.rmcor.reale.hibernate.model.ContactUs;
import com.rmcor.reale.hibernate.model.Login;
import com.rmcor.reale.hibernate.model.Registration;
import com.rmcor.reale.hibernate.model.RmcorContactUs;
import com.rmcor.reale.model.ContactUsModel;
import com.rmcor.reale.model.LoginModel;
import com.rmcor.reale.model.RegistrationModel;
import com.rmcor.reale.service.UserService;
import com.rmcor.reale.utilites.Constants;
import com.rmcor.reale.utilites.ResponseModel;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	public UserDao userDao;
	
	public SendMail sendMail;

	/* method for user registration in service */
	@Override
	public ResponseModel saveUserRegistration(RegistrationModel registrationModel) {
		String loginRes;
		Map<String, Object> res1 = new HashMap<String, Object>();
		ResponseModel res = new ResponseModel();
		Registration registration = new Registration();
		/* method to check mobile number exist or not */
		Integer mobileNumber = checkMobileNumber(registrationModel);
		if (mobileNumber != null && mobileNumber <= 0) {
			/* method to check email id exist or not */
			Integer emailId = checkEmailId(registrationModel);
			if (emailId != null && emailId <= 0) {
				registration = setRegistrationModelToPojo(registrationModel);
				registration = userDao.saveUserRegistration(registration);
				if (registration != null && registration.getPid() != null) {
					/* method to save detail in login table */
					Login login = new Login();
					login = setLoginModelToPojo(registration);
					loginRes = userDao.saveLoginDetail(login);
					res.setMessage(Constants.SUCCESS);
					res.setStatus(Constants.SUCCESSCODE);
					res1.put("data", "User completed a registration process successfully");
					String name_val = registration.getFirstName();
					String email_val = registration.getEmailId();
					new Thread(new Runnable() { 
						public void run() { 
						sendEmailCode111(name_val,email_val);
						}});
					if (loginRes.equalsIgnoreCase(Constants.ERRORCODE)) {
						res.setMessage(Constants.ERROR);
						res.setStatus(Constants.ERRORCODE);
						res1.put("data", "Error in entering the login detail");
					}
				} else {
					res.setMessage(Constants.Registration_Error);
					res.setStatus(Constants.ERRORCODE);
					res1.put("data", "Error in entering the registration detail");
				}
			} else {
				res.setMessage(Constants.Mobile_Number_Exist);
				res.setStatus(Constants.Already_Exist_Code);
				res1.put("data", "Email Id already exist");
			}
		} else {
			res.setMessage(Constants.Mobile_Number_Exist);
			res.setStatus(Constants.Already_Exist_Code);
			res1.put("data", "Mobile number already exist");
		}
		res.setResponse(res1);
		return res;
	}

	/* method to check email id exist or not */
	private Integer checkEmailId(RegistrationModel registrationModel) {
		return userDao.checkEmailId(registrationModel);
	}

	/* method to check mobile number exist or not */
	private Integer checkMobileNumber(RegistrationModel registrationModel) {
		return userDao.checkMobileNumber(registrationModel);
	}

	/* method to set login model to pojo */
	private Login setLoginModelToPojo(Registration source) {
		Login dest = new Login();
		dest.setUserName(source.getUserName());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setEmailId(source.getEmailId());
		dest.setPassword(source.getPassword());
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		dest.setRegisteredUserId(source.getPid());
		dest.setRole(source.getRole());
		dest.setIsActive(1);
		return dest;
	}

	/* method to set registration model to pojo */
	private Registration setRegistrationModelToPojo(RegistrationModel source) {
		Registration dest = new Registration();
		dest.setFirstName(source.getFirstName());
		dest.setMiddleName(source.getMiddleName());
		dest.setLastName(source.getLastName());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setEmailId(source.getEmailId());
		dest.setUserName(source.getFirstName());
		dest.setPassword(source.getPassword());
		dest.setRole(source.getRole());
		dest.setIsActive(1);
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		return dest;
	}

	/* method for user login service */
	@Override
	public ResponseModel userLogin(LoginModel loginModel) {
		ResponseModel res = new ResponseModel();
		Map<String, Object> res1 = new HashMap<String, Object>();
		res.setMessage(Constants.ERROR);
		res.setStatus(Constants.ERRORCODE);
		Login loginResponse = userDao.userLogin(loginModel);
		System.out.print("value of loginResponse "+loginResponse.getPid());
		if(loginResponse!=null && loginResponse.getPid()!=null) {
			res.setMessage(Constants.SUCCESS);
			res.setStatus(Constants.SUCCESSCODE);
			res1.put("data",loginResponse);
			res.setResponse(res1);
		}
		return res;
	}

	@Override
	public ResponseModel saveContactUs(ContactUsModel contactUsModel) {
		Map<String, Object> res1 = new HashMap<String, Object>();
		ResponseModel res = new ResponseModel();
		ContactUs contactUs = new ContactUs();
		contactUs = setContactUsModelToPojo(contactUsModel);
		contactUs = userDao.saveContactUs(contactUs);
		if(contactUs!=null && contactUs.getPid()!=null) {
			res.setMessage(Constants.SUCCESS);
			res.setStatus(Constants.SUCCESSCODE);
			res1.put("data", "Our Team will contact to you very soon.");
		}else {
			res.setMessage(Constants.Contact_Us_Error);
			res.setStatus(Constants.ERRORCODE);
			res1.put("data", "Error in entering the contactus detail");
		}
		
		return res;
	}

	private ContactUs setContactUsModelToPojo(ContactUsModel source) {
		ContactUs dest = new ContactUs();
		dest.setName(source.getName());
		dest.setRegisteredUserId(source.getRegisteredUserId());
		dest.setEmailId(source.getEmailId());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setCountry(source.getCountry());
		dest.setSubject(source.getSubject());
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		return dest;
	}
	
	private RmcorContactUs setRmcorContactUsModelToPojo(ContactUsModel source) {
		RmcorContactUs dest = new RmcorContactUs();
		dest.setName(source.getName());
		dest.setRegisteredUserId(source.getRegisteredUserId());
		dest.setEmailId(source.getEmailId());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setCountry(source.getCountry());
		dest.setSubject(source.getSubject());
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		return dest;
	}
	
	
	private String sendEmailCode111(String name_val, String email_val) {
		System.out.println("enter in send email");
		 // Recipient's email ID needs to be mentioned.
        String to = email_val;
        //String to ="shiluyadav910@gmail.com";

        // Sender's email ID needs to be mentioned
        String from = "shailendrayadav2001@gmail.com";

        // Assuming you are sending email from through gmails smtp
        String host = "smtp.gmail.com";
        
        final String user="shailendrayadav2001@gmail.com";//change accordingly  
        final String password="Amit9yadav@";//change accordingly 

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(user,password);

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Welcome to TuitionWala !");

            // Now set the actual message
            message.setText("Dear "+name_val+", You completed your registration successfully.");
            
         // Send the actual HTML message.
//            message.setContent(
//                   "<h1>Hello +,</h1>"
//                   + "<p>Your registration completed successfully</p>",
//                  "text/html");

            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
		
		return "success";
		
	}

	@Override
	public ResponseModel saveRmcorContactUs(ContactUsModel contactUsModel) {
		Map<String, Object> res1 = new HashMap<String, Object>();
		ResponseModel res = new ResponseModel();
		RmcorContactUs contactUs = new RmcorContactUs();
		contactUs = setRmcorContactUsModelToPojo(contactUsModel);
		contactUs = userDao.saveRmcorContactUs(contactUs);
		if(contactUs!=null && contactUs.getPid()!=null) {
			res.setMessage(Constants.SUCCESS);
			res.setStatus(Constants.SUCCESSCODE);
			res1.put("data", "Our Team will contact to you very soon.");
		}else {
			res.setMessage(Constants.Contact_Us_Error);
			res.setStatus(Constants.ERRORCODE);
			res1.put("data", "Error in entering the contactus detail");
		}
		
		return res;
	}

}
